/*
 * Copyright (c) 2016 Open-RnD Sp. z o.o.
 * Copyright (c) 2020 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <sys/util.h>
#include <sys/printk.h>
#include <sys/__assert.h>
#include <inttypes.h>
#include <kernel.h>
#include <drivers/i2c.h>
#include "lib/lsm6dsox_reg.h"

#define SLEEP_TIME_MS	500
#define PRIORITY 7
#define STACKSIZE 1024

/// Use SW0 - button0
#define SW0_NODE	DT_ALIAS(sw0)
#define I2C_ACC DT_LABEL(DT_NODELABEL(i2c1))

#if !DT_NODE_HAS_STATUS(SW0_NODE, okay)
#error "Unsupported board: sw0 devicetree alias is not defined"
#endif





static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET_OR(SW0_NODE, gpios,
							      {0});
static struct gpio_callback button_cb_data;

typedef struct{
    uint32_t field1;
	uint32_t field2;
	uint32_t field3;
}__attribute__((aligned(4)))dataQueue_s;

K_MSGQ_DEFINE(my_msgq, sizeof(dataQueue_s), 10, 4);

typedef enum{
	BUTTON_NOT_PRESSED = 0,
	BUTTON_PRESSED_ONCE = 1,
	BUTTON_PRESSED_TWICE = 2,
	BUTTON_HOLD = 3,
	PIN_LOW = 0,
	PIN_HIGH = 1
}BUTTON_hardwareStates;

typedef struct{
	uint8_t state;
	uint8_t pin;
	uint8_t numberOfClicks;
	int time;
}BUTTON_buttonStates;

BUTTON_buttonStates buttonStates; 
/*
 * The led0 devicetree alias is optional. If present, we'll use it
 * to turn on the LED whenever the button is pressed.
 */
static struct gpio_dt_spec led0 = GPIO_DT_SPEC_GET_OR(DT_ALIAS(led0), gpios,
						     {0});
static struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET_OR(DT_ALIAS(led1), gpios,
						     {0});
static struct gpio_dt_spec led2 = GPIO_DT_SPEC_GET_OR(DT_ALIAS(led2), gpios,
						     {0});		

/// This fragment of code was used three times so i closed it into function
void initDiode(struct gpio_dt_spec *led){

	int ret = 0;

	if (led->port && !device_is_ready(led->port)) {
		printk("Error %d: LED device %s is not ready; ignoring it\n",
		       ret, led->port->name);
		led->port = NULL;
	}
	if (led->port) {
		ret = gpio_pin_configure_dt(led, GPIO_OUTPUT);
		if (ret != 0) {
			printk("Error %d: failed to configure LED device %s pin %d\n",
			       ret, led->port->name, led->pin);
			led->port = NULL;
		} else {
			printk("Set up LED at %s pin %d\n", led->port->name, led->pin);
			gpio_pin_set_dt(led, 0);
		}
	}
	return;
}

/// Button interrupt callback
void button_led0(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
	dataQueue_s dataQueue;
	uint32_t tempTime = k_cycle_get_32();
	/// CAUTION: pressed button has HIGH state!
	if(buttonStates.pin == PIN_LOW && gpio_pin_get_dt(&button) == PIN_HIGH) 
	{
		buttonStates.pin = PIN_HIGH;
		buttonStates.state = BUTTON_NOT_PRESSED;
		if(200 < k_uptime_get() - buttonStates.time) buttonStates.numberOfClicks = 0;
		buttonStates.time = k_uptime_get();
		printk("Rising edge finished %"PRIu32"ns\n", k_cycle_get_32() - tempTime);

	}else if(buttonStates.pin == PIN_HIGH && gpio_pin_get_dt(&button) == PIN_LOW){
		printk("FallingEdge\n");
		buttonStates.pin = PIN_LOW;
		/// finite state machine depending on the time
		if (180 < k_uptime_get() - buttonStates.time && 500 > k_uptime_get() - buttonStates.time){
			buttonStates.state = BUTTON_PRESSED_ONCE;
			buttonStates.numberOfClicks = 0;
			
		}else if(0 < k_uptime_get() - buttonStates.time && 180 > k_uptime_get() - buttonStates.time){
			if(buttonStates.numberOfClicks == 1){
				buttonStates.state = BUTTON_PRESSED_TWICE;
				buttonStates.numberOfClicks = 0;
			}
			buttonStates.numberOfClicks ++;

		}else if(1000 < k_uptime_get() - buttonStates.time && 3000 > k_uptime_get() - buttonStates.time){
			buttonStates.state = BUTTON_HOLD;

		}else{
			buttonStates.state = BUTTON_NOT_PRESSED;
			buttonStates.numberOfClicks = 0;
		}
		/// Add pulled button to the message queue
		if(BUTTON_NOT_PRESSED != buttonStates.state){
			dataQueue.field1 = buttonStates.state;
			while(k_msgq_put(&my_msgq, &dataQueue, K_NO_WAIT) != 0){
				k_msgq_purge(&my_msgq);
			} 
			printk("Succesfully put data on queue! %"PRIu32"\n", dataQueue.field1);
		}
		printk("Falling edge finished %"PRIu32"ns\n", k_cycle_get_32() - tempTime);
		
		printk("Button pressed at %" PRId64 "ms\n", k_uptime_get());
	}
	
		
}

void executeQueuedEvents(){
	dataQueue_s dataQueue;
	uint8_t state;
	uint8_t diodeState[3] = {0, 0, 0};

	while(1){
		if(0 == k_msgq_get(&my_msgq, &dataQueue, K_FOREVER)){
			printk("Received queued data! %"PRIu32"\n", dataQueue.field1);
			state = dataQueue.field1;
			if(state){
				if(state == BUTTON_PRESSED_ONCE){
					
					if(diodeState[0] == 0){
						gpio_pin_set_dt(&led0, 1);
						diodeState[0] = 1;
					}else{
						gpio_pin_set_dt(&led0, 0);
						diodeState[0] = 0;
					}
				}else if(state == BUTTON_PRESSED_TWICE){
					if(diodeState[1] == 0){
						gpio_pin_set_dt(&led1, 1);
						diodeState[1] = 1;
					}else{
						gpio_pin_set_dt(&led1, 0);
						diodeState[1] = 0;
					}
				}else if(state == BUTTON_HOLD){
					if(diodeState[2] == 0){
						gpio_pin_set_dt(&led2, 1);
						diodeState[2] = 1;
					}else{
						gpio_pin_set_dt(&led2, 0);
						diodeState[2] = 0;
					}
				}
				/// prevention for multiple queued the same event
				uint32_t tempNextMessage = 0;
				while(0 == k_msgq_peek(&my_msgq,&tempNextMessage)){
					if(state == tempNextMessage){
						k_msgq_get(&my_msgq, &tempNextMessage, K_NO_WAIT);	
					}else break;
				}

				state = BUTTON_NOT_PRESSED;
				printk("Current led states... Led0: %d, Led1: %d, Led2: %d\n", diodeState[0], diodeState[1], diodeState[2]);
				k_sleep(K_MSEC(SLEEP_TIME_MS));
			}
		}//Queue Get
	}
}

void blinkInitCallbacks(void){

	gpio_init_callback(&button_cb_data, button_led0, BIT(button.pin));
	gpio_add_callback(button.port, &button_cb_data);

	buttonStates.time = k_cycle_get_32();
	
}

void buttonInit(){
	int ret;
	
	/// button configuration
	ret = gpio_pin_interrupt_configure_dt(&button,
						GPIO_INT_EDGE_BOTH);
	if (ret != 0) {
		printk("Error %d: failed to configure interrupt on %s pin %d\n",
			ret, button.port->name, button.pin);
		return;
	}
	
	if (!device_is_ready(button.port)) {
		printk("Error: button device %s is not ready\n",
		       button.port->name);
		return;
	}

	ret = gpio_pin_configure_dt(&button, GPIO_INPUT);
	if (ret != 0) {
		printk("Error %d: failed to configure %s pin %d\n",
		       ret, button.port->name, button.pin);
		return;
	}
	printk("Set up button at %s pin %d\n", button.port->name, button.pin);
	
}

K_THREAD_DEFINE(blink0_id, STACKSIZE, executeQueuedEvents, NULL, NULL, NULL,
		PRIORITY, 0, 0);

static int32_t platform_write(void *handle, uint8_t reg, const uint8_t *bufp, uint16_t len){
	uint8_t res = 0;
	for(uint8_t i = 0; i < len; i++){
		if(0 == (res = i2c_reg_write_byte(handle, LSM6DSOX_I2C_ADDR, reg, bufp[i])));
		else return res;
	}
	return res;
}
static int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len	){
	int32_t ret = 0;
	if(0 == (ret = i2c_reg_read_byte(handle, LSM6DSOX_I2C_ADDR, reg, bufp))) printk("Successfully read I2C data %" PRIu8"\n", bufp[0]);
	return ret;
}
static void tx_com( uint8_t *tx_buffer, uint16_t len );

void main(void)
{
	const struct device *i2c_dev = device_get_binding(DT_LABEL(DT_NODELABEL( i2c0 )));
	stmdev_ctx_t dev_ctx;
	lsm6dsox_all_sources_t all_source;
	uint8_t buffer[10];
	dev_ctx.write_reg = platform_write;
  	dev_ctx.read_reg = platform_read;
	dev_ctx.handle = i2c_dev;

	
	initDiode(&led0);
	initDiode(&led1);
	initDiode(&led2);
	buttonInit();
	

	if (i2c_dev == NULL) {
    printk("I2C: Device driver not found\n");
  	}
  	else{
		printk("I2C: Device driver found!!!\n"); 

		if(0 == i2c_configure(i2c_dev, I2C_SPEED_SET(I2C_SPEED_STANDARD) | I2C_MODE_MASTER)) printk("I2C Configure success!\n");

		uint8_t dummy;
		lsm6dsox_device_id_get(&dev_ctx, &dummy);
		if(LSM6DSOX_ID == dummy) printk("LSM6DSOX ID found %"PRIu8"\n", dummy);
	}
	
	lsm6dsox_reset_set(&dev_ctx, PROPERTY_ENABLE);
	/* Disable I3C interface */
	lsm6dsox_i3c_disable_set(&dev_ctx, LSM6DSOX_I3C_DISABLE);
	/* Set XL and Gyro Output Data Rate */
	lsm6dsox_xl_data_rate_set(&dev_ctx, LSM6DSOX_XL_ODR_208Hz);
	/* Set 2g full XL scale and 250 dps full Gyro */
	lsm6dsox_xl_full_scale_set(&dev_ctx, LSM6DSOX_2g);
	lsm6dsox_gy_full_scale_set(&dev_ctx, LSM6DSOX_250dps);
	/* Set duration for Activity detection to 9.62 ms (= 2 * 1 / ODR_XL) */
	lsm6dsox_wkup_dur_set(&dev_ctx, 0x2);
	
	/* Set duration for Inactivity detection to 4.92 s (= 2 * 512 / ODR_XL) */
	lsm6dsox_act_sleep_dur_set(&dev_ctx, 0x02);
	/* Set Activity/Inactivity threshold to 62.5 mg */
	lsm6dsox_wkup_threshold_set(&dev_ctx, 0x02);
	/* Inactivity configuration: XL to 12.5 in LP, gyro to Power-Down */
	lsm6dsox_act_mode_set(&dev_ctx, LSM6DSOX_XL_12Hz5_GY_PD);


	blinkInitCallbacks();
	printk("Press the button\n");
	

	
}
